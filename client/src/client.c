#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <sys/un.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include "rtclm/client.h"
#include "rtclm/logging.h"
#include "rtclm/messages.h"

#define MAX_SEND_TRIES  5
#define MAX_RECV_TRIES  5
#define RECV_WAIT_US    1000
#define SEND_WAIT_US    1000


static int send_command(
  rtclm_client_t *client,
  command_t command,
  const void *buf, unsigned size)
{
  uint8_t udp_payload[UDP_PAYLOAD_SIZE];
  packet_t *packet = (packet_t*)udp_payload;

  //_I("Sending with ID: %u", client->id);
  packet->id      = client->id;
  packet->command = command;
  memcpy(packet->payload, buf, size);

  return sendto(
    client->socket,
    packet, sizeof(command_t) + size,
    MSG_CONFIRM,
    (const struct sockaddr*)&(client->server_addr),
    sizeof(client->server_addr));
}


static inline int register_client(rtclm_client_t *client){
  int ret;
  unsigned stries, rtries;
  uint8_t udp_payload[UDP_PAYLOAD_SIZE];

  /* Set client state as requesting initialization */
  client->state = CLIENT_STATE_REQUEST_INIT;

  for(stries=0; stries<MAX_SEND_TRIES; stries++){
    _D("Sending register request");
    ret = send_command(client, RTCLM_CMD_REGISTER, client->name, strlen(client->name)+1);
    if(ret == -1){
      _SE("Failed to register \"%s\" client", client->name);
      return -1;
    }

    _D("Receiving register acknowledgment");
    for(rtries=0; rtries<MAX_RECV_TRIES; rtries++){
      ret = recv(client->socket,
        udp_payload, UDP_PAYLOAD_SIZE, MSG_DONTWAIT);

      if(ret != -1){
        break;
      }

      usleep(RECV_WAIT_US);
    }

    /* Retreived acknowledgment */
    _D("Retreived acknowledgment");
    if(ret != -1){
      packet_t *packet = (packet_t*)udp_payload;
      _D("Received message: %s (%d bytes)", (char*)packet->payload, ret);
      client->id = *(uint16_t*)packet->payload;
      client->state = CLIENT_STATE_INITIALIZED;
      return 0;
    }

    usleep(SEND_WAIT_US);
  }

  return -1;
}

static inline int unregister_client(rtclm_client_t *client){
  int ret;
  unsigned stries, rtries;
  uint8_t udp_payload[UDP_PAYLOAD_SIZE];

  /* Set client state as requesting initialization */
  client->state = CLIENT_STATE_REQUEST_DEINIT;

  for(stries=0; stries<MAX_SEND_TRIES; stries++){
    /* Send unregister request */
    ret = send_command(client, RTCLM_CMD_UNREGISTER, &client->id, sizeof(client->id));
    if(ret == -1){
      _SE("Failed to unregister \"%s\" client", client->name);
      return -1;
    }

    /* Receiving unregister acknowledgment */
    for(rtries=0; rtries<MAX_RECV_TRIES; rtries++){
      ret = recv(client->socket,
        udp_payload, UDP_PAYLOAD_SIZE, MSG_DONTWAIT);

      if(ret != -1){
        break;
      }

      usleep(RECV_WAIT_US);
    }

    /* Retreived acknowledgment */
    if(ret != -1){
      packet_t *packet = (packet_t*)udp_payload;
      _D("Received message: %d bytes", ret);
      client->state = CLIENT_STATE_DEINITIALIZED;
      return 0;
    }

    usleep(SEND_WAIT_US);
  }

  return -1;
}



rtclm_client_t* rtclm_client_init(const char *name, const char *ip, uint16_t port){
  rtclm_client_t *client;
  int ret;

  _D("Initializing RTCLM client \"%s\" at \"%s:%u\"", name, ip, port);
  client = malloc(sizeof(rtclm_client_t));
  if(client == NULL){
    _SE("Failed to initialize client");
    return NULL;
  }

  _D("Storing connection attributes");
  client->name = strdup(name);
  if(client->name == NULL){
    _SE("Faile to allocate memory");
    goto failure_strdup_0;
  }

  client->ip = strdup(ip);
  if(client->ip == NULL){
    _SE("Faile to allocate memory");
    goto failure_strdup_1;
  }

  client->state = CLIENT_STATE_IDLE;
  client->port  = port;
  memset(&client->server_addr, 0, sizeof(client->server_addr)); 
  client->server_addr.sin_family      = AF_INET;
  client->server_addr.sin_port        = htons(port);
  client->server_addr.sin_addr.s_addr = INADDR_ANY;

  _D("Initializing socket for \"%s:%u\"", ip, port); 
  client->socket = socket(AF_INET, SOCK_DGRAM, 0);
  if(client->socket == -1){
    _SE("Failed to create socket\n");
    goto failure_socket;
  }

  _D("Converting ip address from text to binary");
  if(inet_pton(AF_INET, ip, &client->server_addr.sin_addr) <= 0){
    _E("Failed to convert ip address to binary form");
    goto failure_inet_pton;
  }

  _D("Registering \"%s\" client", name);
  if(register_client(client) == -1){
    _E("Failed to register client \"%s\"", name);
    goto failure_register_client;
  }

  return client;


failure_register_client:
failure_inet_pton:
  shutdown(client->socket, SHUT_RDWR);
failure_socket:
  free(client->ip);
failure_strdup_1:
  free(client->name);
failure_strdup_0:
  free(client);
  return NULL;
}

void rtclm_client_deinit(rtclm_client_t *client){
  _D("Deinitializing RTCLM client at \"%s:%u\"", client->ip, client->port);
  shutdown(client->socket, SHUT_RDWR);
  free(client->ip);
  free(client->name);
  free(client);
}

static inline uint64_t retreive_timestamp_nsec(){
  struct timespec ts;

  if(clock_gettime(CLOCK_REALTIME, &ts) == -1){
    _W("Failed to retreive timestamp");
    return 0;
  }

  return (uint64_t)(1e9*ts.tv_sec) + (uint64_t)(ts.tv_nsec);
}

int rtclm_client_notify_start(rtclm_client_t *client){
  uint64_t ts_nsec = retreive_timestamp_nsec();

  int ret = send_command(client, RTCLM_CMD_NOTIFY_LOOP_START, &ts_nsec, sizeof(ts_nsec));
  if(ret == -1){
    _SE("Failed to notify-loop-start for \"%s\" client", client->name);
    return -1;
  }
  return 0;
}

int rtclm_client_notify_stop(rtclm_client_t *client){
  uint64_t ts_nsec = retreive_timestamp_nsec();

  int ret = send_command(client, RTCLM_CMD_NOTIFY_LOOP_STOP, &ts_nsec, sizeof(ts_nsec));
  if(ret == -1){
    _SE("Failed to notify-loop-stop for \"%s\" client", client->name);
    return -1;
  }
  return 0;
}

int rtclm_client_notify_emit(rtclm_client_t *client){
  uint64_t ts_nsec = retreive_timestamp_nsec();

  int ret = send_command(client, RTCLM_CMD_NOTIFY_LOOP_EMIT, &ts_nsec, sizeof(ts_nsec));
  if(ret == -1){
    _SE("Failed to notify-emit for \"%s\" client", client->name);
    return -1;
  }
  return 0;
}
