#ifndef _RTCLM_CLIENT_H_
#define _RTCLM_CLIENT_H_

/* C-linkage when included from C++ - BEGIN */
#ifdef __cplusplus
  extern "C" {
#endif


#include <stdint.h>
#include <arpa/inet.h>
#include "rtclm/status.h"

/* RTCLM client's state machine */
typedef enum {
  CLIENT_STATE_IDLE=0,
  CLIENT_STATE_REQUEST_INIT,
  CLIENT_STATE_INITIALIZED,
  CLIENT_STATE_CLOOP_ACTIVE,
  CLIENT_STATE_CLOOP_CLOOP,
  CLIENT_STATE_REQUEST_DEINIT,
  CLIENT_STATE_DEINITIALIZED,
} client_state_t;

/* RTCLM client's struct object */
typedef struct {
  int                 socket;
  char               *ip;
  char               *name;
  uint16_t            id;
  uint16_t            port;
  struct sockaddr_in  server_addr;
  client_state_t      state;
} rtclm_client_t;

/* TODO: documentation */
rtclm_client_t* rtclm_client_init(const char *name, const char *ip, uint16_t port);

/* TODO: documentation */
void rtclm_client_deinit(rtclm_client_t *client);

/* TODO: documentation */
int rtclm_client_notify_start(rtclm_client_t *client);
int rtclm_client_notify_stop(rtclm_client_t *client);
int rtclm_client_notify_emit(rtclm_client_t *client);


/* C-linkage when included from C++ - STOP */
#ifdef __cplusplus
  }
#endif

#endif
