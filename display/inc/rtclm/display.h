#ifndef _DISPLAY_H_
#define _DISPLAY_H_

#include <pthread.h>
#include <dirent.h>
#include <GLFW/glfw3.h>
#include "rtclm/status.h"
#include "imgui/imgui.h"

typedef struct {
  GLFWwindow  *window;
  DIR         *shm_dir;
  pthread_t    pid;
} rtclm_display_t;

rtclm_display_t* rtclm_display_init(void);
void rtclm_display_deinit(rtclm_display_t *display);

#define WINDOW_RES_H 800
#define WINDOW_RES_V 600
#define WINDOW_TITLE "RTCLM display"

#endif // _DISPLAY_H_
