#include <cstddef>
#include <pthread.h>
#include <dirent.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <GLFW/glfw3.h>

/* dearGui */
#include "imgui/imgui.h"
#include "imgui/imgui_impl_glfw.h"
#include "imgui/imgui_impl_opengl3.h"
#include "implot/implot.h"

/* rtclm */
#include "rtclm/display.h"
#include "rtclm/logging.h"
#include "rtclm/config.h"
#include "rtclm/messages.h"
#include "rtclm/utils.h"

#define RTCLM_WAIT_US 500000

/* Linked list for tracking all the components */
typedef struct llist {
  struct llist  *next;
  char          *name;
  char          *path;
  int            fd;
  statistics_t  *stats;
} llist_t;

/* constants */
const ImVec4 g_clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

/* globals */
llist_t *head;
int g_window_width;
int g_window_height;

/********************* LLIST API - BEGIN *********************/
static int llist_compare_by_name(llist_t *shm, const void *pdata){
  char *name = (char*)pdata;
  return (strcmp(shm->name, (const char*)pdata) == 0) ? 1 : 0;
}

static llist_t** llist_find( int(*compare_handler)(llist_t*,const void*), const void *pdata){
  llist_t **shm;

  /* Find component */
  for(shm=&head; *shm!=NULL; shm=&((*shm)->next)){
    if(compare_handler(*shm, pdata)){
      break;
    }
  }

  /* Component may be either found or not */
  return shm;
}

static rtclmStatus_t llist_create_and_add(const char *name){
  int ret;
  llist_t *shm;
  rtclmStatus_t status = RTCLM_FAILURE;
  uint32_t shm_size;

  shm = (llist_t*)malloc(sizeof(llist_t));
  if(!shm){
    _SE("Failed to allocate memory");
    return RTCLM_NO_MEM;
  }

  _D("Constructing shm object's \"%s\" name", name);
  shm->name = strdup(name);
  if(!shm->name){
    _SE("Failed to allocate memory");
    status = RTCLM_NO_MEM;
    goto failure_strdup;
  }

  _D("Constructing shm objet's path \"%s\"", name);
  shm->path = utils_construct_string("%s", name);
  if(shm->path == NULL){
    _E("Failed to construct shm path for \"%s\"", name);
    status = RTCLM_NO_MEM;
    goto failure_path;
  }

  _D("Initializing SHM at \"%s\"", shm->path);
  shm->fd = shm_open(shm->path, O_RDWR, S_IRUSR | S_IWUSR);
  if(shm->fd == -1){
    _SE("Failed to open SHM: \"%s\"", shm->path);
    status = RTCLM_FAILURE;
    goto failure_open;
  }

  _D("Mapping SHM memory for \"%s\"", name);
  shm_size = utils_align_u32(
    sizeof(statistics_t)+sizeof(uint64_t)*RTCLM_TRACKED_SAMPLE_COUNT, 0x1000);

  shm->stats = (statistics_t*)mmap(NULL,
    shm_size,
    PROT_READ | PROT_WRITE,
    MAP_SHARED,
    shm->fd,
    0);

  if(shm->stats == ((void*)-1)){
    _SE("Failed to memory map shared memory area for \"%s\"", shm->path);
    status = RTCLM_FAILURE;
    goto failure_mmap;
  }

  shm->next = head;
  head = shm;
  return RTCLM_SUCCESS;

failure_mmap:
  close(shm->fd);
failure_open:
  free(shm->path);
failure_path:
  free(shm->name);
failure_strdup:
  free(shm);
  return status;
}


/********************* LLIST API - END *********************/


static void glfw_error_callback(int error, const char* description){
  fprintf(stderr, "Glfw Error %d: %s\n", error, description);
}

void shm_lookup(rtclm_display_t *display){
  /* Check for any unacounted FIFOs */
  struct dirent *dirent;
  while((dirent = readdir(display->shm_dir)) != NULL){
    if(dirent->d_type == DT_REG && strstr(dirent->d_name,"rtclm")){
      /* Check if shm is already registered (TODO: inefficient) */
      llist_t **shm = llist_find(llist_compare_by_name, dirent->d_name);
      if(*shm != NULL){
        continue;
      }

      /* Create node in the linked list */
      _D("Creating - %s", dirent->d_name);
      if(llist_create_and_add(dirent->d_name) != RTCLM_SUCCESS){
        _W("Failed to create aggregator object");
        continue;
      }
    }
  }
  rewinddir(display->shm_dir);
}

static void custom_Text_time(const char *title, uint64_t val_ns){
  if(val_ns > 1e8){         //0.1 s
    ImGui::Text("%s: %.4f s", title, ((float)val_ns)/1.0e9);
  } else if(val_ns > 1e5){
    ImGui::Text("%s: %.2f ms", title, ((float)val_ns)/1.0e6);
  } else if(val_ns > 1e2){
    ImGui::Text("%s: %.0f us", title, ((float)val_ns)/1.0e3);
  } else{
    ImGui::Text("%s: %.0f ns", title, (float)val_ns);
  }
}

/* Export statistics to .csv file */
static inline void export_statistics(llist_t *aggr, char *file_name){
  statistics_t *stats = aggr->stats;
  const char *header  = "name, maximum, minimum, average, std_deviation, number_of_samples, last_timestamp\n";
  
  /* The header is not required if the file already exists */
  if (access(file_name, F_OK) == 0) {
    header = "";
  } 

  if (strlen(file_name) > 0){
    FILE *fp = fopen(file_name, "a");
    
    fprintf(fp, "%s%s, %.4f, %.4f, %.4f, %.4f, %lu, %lu\n",
      header,
      aggr->name,
      (double)stats->maximum/1000000, // us to ms
      (double)stats->minimum/1000000,
      (double)stats->average/1000000,
      (double)stats->std_deviation/1000000,
      stats->number_of_samples,
      stats->last_timestamp
    );
    
    fclose(fp);
    stats->status = SHM_DEFAULT_STATUS;
  }
}

#define CHILD_HEIGHT  165
#define CHILD_WIDTH   160
#define PLOT_WIDTH_CORRECTION   (CHILD_WIDTH+20)
#define PLOT_HEIGHT             120

static inline void draw_gui(){
  int window_index = 0;
  static char file_name[50] = "statistics.csv";

  ImGui::SetNextWindowPos(ImVec2(0,0));
  ImGui::SetNextWindowSize(ImVec2(g_window_width, g_window_height), ImGuiCond_Always);
  ImGui::Begin("##Main Window", 0, ImGuiWindowFlags_NoResize
    | ImGuiWindowFlags_NoMove
    | ImGuiWindowFlags_NoCollapse);

  /* Reset button sets the status of all components to 1 */
  if (ImGui::Button("Reset stats")){
    llist_t *shm = head;
    while(shm){
      shm->stats->status = SHM_RESET_STATS;

      shm = shm->next;
    }
  }

  ImGui::SameLine();

  /* Export button sets the status of all components to 2 */
  if (ImGui::Button("Export stats")){
    llist_t *shm = head;
    while(shm){
      shm->stats->status = SHM_EXPORT_STATS;

      if(shm->stats->status == SHM_EXPORT_STATS){
        export_statistics(shm, file_name);
      }

      shm = shm->next;
    }
  }
  
  ImGui::SameLine();

  /* Text input field for export file name */
  ImGui::PushItemWidth(120);
  ImGui::InputTextWithHint("##label", "file_name.csv", file_name, IM_ARRAYSIZE(file_name));
  
  llist_t *shm = head;
  while(shm){
    ImVec2 window_size = ImVec2(g_window_width, CHILD_HEIGHT);
    ImVec2 child0_size = ImVec2(CHILD_WIDTH, CHILD_HEIGHT);
    ImVec2 child1_size = ImVec2(g_window_width-PLOT_WIDTH_CORRECTION, PLOT_HEIGHT);
    ImVec2 window_pos  = ImVec2(0, window_index*CHILD_HEIGHT);
    ImVec2 child0_pos  = ImVec2(0, window_index*CHILD_HEIGHT);
    ImVec2 child1_pos  = ImVec2(CHILD_WIDTH+10, window_index*CHILD_HEIGHT+30);

    ImGui::PushID(shm->name);
    if(ImGui::CollapsingHeader(shm->name, ImGuiTreeNodeFlags_DefaultOpen)){

      //ImGui::SetNextWindowSize(child0_size, ImGuiCond_Always); // No effect on groups
      ImGui::BeginGroup();
        //ImGui::PushItemWidth(CHILD_WIDTH);
        custom_Text_time("Maximum", shm->stats->maximum);
        custom_Text_time("Minimum", shm->stats->minimum);
        custom_Text_time("Average", shm->stats->average);
        custom_Text_time("STD deviation", shm->stats->std_deviation);
        custom_Text_time("Last cycle", shm->stats->samples[shm->stats->last_index]);
        ImGui::Text("Samples: %lu", shm->stats->number_of_samples);
        ImGui::Text("                       "); /* TODO: Very dumb workaround */
        //ImGui::PopItemWidth();
      ImGui::EndGroup();
      ImGui::SameLine();

      ImGui::BeginGroup();
        if(ImPlot::BeginPlot("##Histogram", child1_size)){
          ImPlot::SetupAxes(NULL, "Occurence", ImPlotAxisFlags_AutoFit,ImPlotAxisFlags_AutoFit);
          ImPlot::SetNextFillStyle(IMPLOT_AUTO_COL, 0.5f);
          ImPlot::PlotHistogram(
            "##Control loop length",        /* Legend / ID */
            (uint32_t*)shm->stats->samples, /* value array */
            shm->stats->number_of_samples,  /* number of elements */
            DISP_HISTOGRAM_BINS,            /* number of bins */
            1.0,                            /* value offset */
            /* A temporary solution for displaying data that may not follow a Gaussian distribution */
            ImPlotRange()
            // ImPlotRange(
            //   shm->stats->average-DISP_HISTOGRAM_STDDEV_RANGE*shm->stats->std_deviation,
            //   shm->stats->average+DISP_HISTOGRAM_STDDEV_RANGE*shm->stats->std_deviation)
          );
        ImPlot::EndPlot();
        }
      ImGui::EndGroup();
    }
    ImGui::PopID();

    window_index++;
    shm = shm->next;
  }
  ImGui::End();
}

void display_cleanup(void *args){
  rtclm_display_t *display = (rtclm_display_t*)args;

  ImGui_ImplOpenGL3_Shutdown();
  ImGui_ImplGlfw_Shutdown();
  ImPlot::DestroyContext();
  ImGui::DestroyContext();

  glfwDestroyWindow(display->window);
  glfwTerminate();
}

void *display_thread_entry(void *args){
  rtclm_display_t *display = (rtclm_display_t*)args;


  _D("Waiting for shared memory directory to be created");
  do{
    display->shm_dir = opendir("/dev/shm");
    usleep(RTCLM_WAIT_US);
  } while(!display->shm_dir);

  _D("Setting GLGW error callback");
  glfwSetErrorCallback(glfw_error_callback);
  if(!glfwInit()){
    _E("Failed to set GLFW error callback");
    return NULL;
  }

  _D("Hinting OpenGL shader language version");
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

  _D("Creating GLFW window");
  display->window = glfwCreateWindow(WINDOW_RES_H, WINDOW_RES_V, WINDOW_TITLE, NULL, NULL);
  if(display->window == NULL){
    _E("Failed to create GLFW window");
    return NULL;
  }
  glfwMakeContextCurrent(display->window); //
  glfwSwapInterval(1); // Enable vsync

  _D("Initializing Dear ImGui context");
  IMGUI_CHECKVERSION();
  ImGui::CreateContext();
  ImPlot::CreateContext();
  static ImGuiIO &io = ImGui::GetIO(); (void)io;

  _D("Setting Dear ImGui style");
  ImGui::StyleColorsDark();

  _D("Setting up Platform/Renderer backends");
  ImGui_ImplGlfw_InitForOpenGL(display->window, true);
  const char* glsl_version = "#version 130";
  ImGui_ImplOpenGL3_Init(glsl_version);

  _D("Registering cleanup handler");
  pthread_cleanup_push(display_cleanup, display);

  while (!glfwWindowShouldClose(display->window)){

    /* Poll events */
    glfwPollEvents();

    /* Start Dear ImGui frame */
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();

    /* Retreive window size */
    glfwGetWindowSize(display->window, &g_window_width, &g_window_height);

    /* Update shared memory lookup */
    shm_lookup(display);

    /* Draw GUI */
    draw_gui();

    _D("Rendering");
    ImGui::Render();
    int display_w, display_h;
    glfwGetFramebufferSize(display->window, &display_w, &display_h);
    glViewport(0, 0, display_w, display_h);
    glClearColor(
      g_clear_color.x * g_clear_color.w, // red channel
      g_clear_color.y * g_clear_color.w, // green channel
      g_clear_color.z * g_clear_color.w, // blue channel
      g_clear_color.w);                  // alpha
    glClear(GL_COLOR_BUFFER_BIT);
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

    _D("Swapping buffers");
    glfwSwapBuffers(display->window);
  }

  pthread_cleanup_pop(1);
  return NULL;
}


rtclm_display_t* rtclm_display_init(void){
  rtclm_display_t *display;
  int ret;

  _D("Initializing RTCLM display object");
  display = (rtclm_display_t*)malloc(sizeof(rtclm_display_t));
  if(display == NULL){
    _E("Failed to allocate RTCLM display object");
    return NULL;
  }

  _I("Launching RTCLM display thread");
  ret = pthread_create(&display->pid, NULL, display_thread_entry, display);
  if(ret != 0){
    _E("Failed to initialize display thread");
    free(display);
    return NULL;
  }

  return display;
}

void rtclm_display_deinit(rtclm_display_t *display){
  if(pthread_cancel(display->pid) != 0){
    _W("Failed to cancel display thread");
  }

  if(pthread_join(display->pid, NULL) != 0){
    _W("Failed to join display thread");
  }

  if(closedir(display->shm_dir) == -1){
    _SW("Failed to close display's shm directory");
  }

  free(display);
}
