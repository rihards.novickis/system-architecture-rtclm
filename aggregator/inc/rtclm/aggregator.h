#ifndef _AGGREGATOR_H_
#define _AGGREGATOR_H_

/* C-linkage when included from C++ - BEGIN */
#ifdef __cplusplus
  extern "C" {
#endif


#include <dirent.h>
#include <pthread.h>

typedef struct {
  pthread_t  pid;
  DIR *dir;
} rtclm_aggregator_t;


/* TODO: Documentation */
rtclm_aggregator_t* rtclm_aggregator_init();

/* TODO: Documentation */
void rtclm_aggregator_deinit(rtclm_aggregator_t *aggregator);


/* C-linkage when included from C++ - STOP */
#ifdef __cplusplus
  }
#endif

#endif
