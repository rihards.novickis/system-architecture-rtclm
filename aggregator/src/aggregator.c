#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <errno.h>
#include <fcntl.h>
#include <math.h>
#include <dirent.h>
#include <pthread.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>

#include "rtclm/aggregator.h"
#include "rtclm/status.h"
#include "rtclm/config.h"
#include "rtclm/utils.h"
#include "rtclm/logging.h"
#include "rtclm/messages.h"
#include "rtclm/ipc.h"


/* Linked list for tracking all the components*/
typedef struct llist {
  struct llist      *next;
  char              *name;
  rtclm_ipc_fifo_t  *fifo;
  rtclm_ipc_shm_t   *shm;
  statistics_t      *stats;
} llist_t;

/* Head of the llist */
static llist_t *head;


/********************* LLIST API - BEGIN *********************/
static int llist_compare_by_name(llist_t *iface, const void *pdata){
  char *name = (char*)pdata;
  return (strcmp(iface->name, pdata) == 0) ? 1 : 0;
}


static llist_t** llist_find( int(*compare_handler)(llist_t*,const void*), const void *pdata){
  llist_t **iface;

  /* Find component */
  for(iface=&head; *iface!=NULL; iface=&((*iface)->next)){
    if(compare_handler(*iface, pdata)){
      break;
    }
  }

  /* Component may be either found or not */
  return iface;
}

static rtclmStatus_t llist_create_and_add(const char *name){
  int ret;
  llist_t *aggr;
  rtclmStatus_t status = RTCLM_FAILURE;

  aggr = (llist_t*)malloc(sizeof(llist_t));
  if(!aggr){
    _SE("Failed to allocate memory");
    return RTCLM_NO_MEM;
  }

  _D("Constructing aggregator object \"%s\"", name);
  aggr->name = strdup(name);
  if(!aggr->name){
    _SE("Failed to allocate memory");
    status = RTCLM_NO_MEM;
    goto failure_strdup;
  }

  _D("Initializing FIFO interface \"%s/%s\"", FIFO_TMP_DIRECTORY, name);
  aggr->fifo = rtclm_ipc_fifo_init(FIFO_TMP_DIRECTORY, name, 0777, O_RDONLY | O_NONBLOCK);
  if(aggr->fifo == NULL){
    _E("Failed to initialize FIFO interface \"%s\"", name);
    goto failure_fifo_init;
  }

  _D("Initializing SHM interface \"/%s\"", name);
  uint32_t shm_size = utils_align_u32(
    sizeof(statistics_t)+sizeof(uint32_t)*RTCLM_TRACKED_SAMPLE_COUNT, 0x1000);

  aggr->shm = rtclm_ipc_shm_init(
    name,
    S_IRUSR | S_IWUSR,
    O_RDWR | O_CREAT,
    shm_size);

  if(aggr->shm == NULL){
    goto failure_ipc_shm_init;
  }

  aggr->stats = (statistics_t*)aggr->shm->mem;
  aggr->stats->maximum = 0;
  aggr->stats->minimum = 0xffffffffffffffff;
  aggr->stats->last_timestamp    = 0;
  aggr->stats->last_index        = RTCLM_TRACKED_SAMPLE_COUNT;
  aggr->stats->number_of_samples = 0;
  aggr->stats->std_deviation     = 0;

  aggr->next = head;
  head = aggr;
  return RTCLM_SUCCESS; 


failure_ipc_shm_init:
  rtclm_ipc_fifo_deinit(aggr->fifo);
failure_fifo_init:
  free(aggr->name);
failure_strdup:
  free(aggr);
  return status;
}


/********************* LLIST API - END *********************/

static inline void reset_statistics(statistics_t *stats){
      stats->maximum = 0;
      stats->minimum = 0xffffffffffffffff;
      stats->last_timestamp    = 0;
      stats->last_index        = RTCLM_TRACKED_SAMPLE_COUNT;
      stats->number_of_samples = 0;
      stats->std_deviation     = 0;
      stats->status            = SHM_DEFAULT_STATUS;
}


static inline void update_statistics(llist_t *aggr, notify_t *note){
  statistics_t *stats = aggr->stats;
  uint64_t duration;

  switch(note->type){
    case RTCLM_CMD_NOTIFY_LOOP_START:
      _D("Retreived command: RTCLM_NOTIFY_LOOP_START");
      stats->last_timestamp = note->timer;
      return; /* return, because we also require stop command */

    case RTCLM_CMD_NOTIFY_LOOP_STOP:
      _D("Retreived command: RTCLM_NOTIFY_LOOP_STOP");
      duration = note->timer - stats->last_timestamp;
      stats->last_timestamp = note->timer;
      break;

    case RTCLM_CMD_NOTIFY_LOOP_EMIT:
      _D("Retreived command: RTCLM_NOTIFY_LOOP_EMIT");
      if((stats->number_of_samples == 0) && (stats->last_timestamp == 0)){
        stats->last_timestamp = note->timer;
        return;
      }

      duration = note->timer - stats->last_timestamp;
      stats->last_timestamp = note->timer;
      break;

    default:
      _W("Unrecognized command duting aggregation");
      return;
  }
  
  /* reset statistics */
  if(stats->status == SHM_RESET_STATS){
    reset_statistics(stats);
  }
  
  /* update maximum */
  if(stats->maximum < duration){
    stats->maximum = duration;
  }

  /* update minimum */
  if(stats->minimum > duration){
    stats->minimum = duration;
  }

  /* update number of samples */
  if(stats->number_of_samples < RTCLM_TRACKED_SAMPLE_COUNT){
    stats->number_of_samples++;
  }

  /* update last timestamp index */
  if(stats->last_index < RTCLM_TRACKED_SAMPLE_COUNT-1){
    stats->last_index++;
  } else {
    stats->last_index = 0;
  }

  /* update samples */
  stats->samples[stats->last_index] = (uint32_t)duration;

  /* update average */
  uint64_t total = 0;
  for(int i=0; i<stats->number_of_samples; i++){
    total += stats->samples[i];
  }
  stats->average = total/stats->number_of_samples;

  /* update standard deviation */
  uint64_t std = 0;
  for(int i=0; i<stats->number_of_samples; i++){
    std += (stats->samples[i] - stats->average)
          *(stats->samples[i] - stats->average);
  }
  std /= stats->number_of_samples;
  stats->std_deviation = sqrt(std);
}


void rtclm_aggregator_thread_cleanup(void *pdata){
  llist_t *next;
  while(head != NULL){
    next = head->next;
    rtclm_ipc_shm_deinit(head->shm);
    rtclm_ipc_fifo_deinit(head->fifo);
    free(head->name);
    free(head);
    head = next;
  }
}


void* rtclm_aggregator_thread(void *pdata){
  rtclm_aggregator_t *aggregator;

  /* Thread's private data structure */
  aggregator = (rtclm_aggregator_t*)pdata;

  _D("Registering server thread's cleanup handler");
  pthread_cleanup_push(rtclm_aggregator_thread_cleanup, pdata);

  _D("Waiting for FIFO directory to be created");
  do{
    aggregator->dir = opendir(FIFO_TMP_DIRECTORY);
    usleep(RTCLM_AGGREGATOR_WAIT_US);
  } while(!aggregator->dir);

  _D("Entering aggregator process loop");
  while(1){
    /* Check for any unacounted FIFOs */
    struct dirent *dirent;
    while((dirent = readdir(aggregator->dir)) != NULL){
      if(dirent->d_type == DT_FIFO){

        /* Check if FIFO is already registered (TODO: inefficient) */
        llist_t **fifo = llist_find(llist_compare_by_name, dirent->d_name);
        if(*fifo != NULL){
          continue;
        }

        /* Create node in the linked list */
        if(llist_create_and_add(dirent->d_name) != RTCLM_SUCCESS){
          _W("Failed to create aggregator object");
          continue;
        }
      }
    }

    /* Aggregate data and fill shared memory structure */
    llist_t *aggr = head;
    while(aggr != NULL){
      notify_t notes[AGGR_MAX_NOTES_PER_READ];

      int ret = read(aggr->fifo->fd, notes, sizeof(notes));
      if(ret == -1 && errno != EAGAIN){
        _E("Failed to read aggregate fifo");
      }

      int note_index = 0;
      while(ret > 0){
        _D("Updating statistics for: \"%s\"", aggr->name);
        update_statistics(aggr, &notes[note_index++]);
        ret -= sizeof(notify_t);
      }

      aggr = aggr->next;
    }


    rewinddir(aggregator->dir);

    /* TODO: Aggregate inputs from the FIFOs and create statistics */

    /* Pause */
    usleep(RTCLM_AGGREGATOR_WAIT_US);
  }
 
  /* Pthread's cleanup handler */
  pthread_cleanup_pop(1);

  return NULL;
}


rtclm_aggregator_t* rtclm_aggregator_init(){
  rtclm_aggregator_t *aggregator;

  _D("Initializing RTCLM aggregator");
  aggregator = malloc(sizeof(rtclm_aggregator_t));
  if(aggregator == NULL){
    _SE("Failed to initialize server");
    return NULL;
  }

  _D("Starting aggregator thread");
  int ret = pthread_create(&aggregator->pid, 0, rtclm_aggregator_thread, aggregator);
  if(ret != 0){
    _E("Failed to initialize server pthread (error: %d)", ret);
    goto failure_pthread_create;
  }
  
  return aggregator;

failure_pthread_create:
  free(aggregator);
}


void rtclm_aggregator_deinit(rtclm_aggregator_t *aggregator){
  pthread_cancel(aggregator->pid);
  pthread_join(aggregator->pid, NULL);
  free(aggregator);
}
