#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>

#include "rtclm/ipc.h"
#include "rtclm/utils.h"
#include "rtclm/logging.h"


rtclm_ipc_fifo_t* rtclm_ipc_fifo_init(
  const char  *dpath,
  const char  *name,
  mode_t       mode,
  int          flags)
{

  _D("Allocating memory for RTCLM notifier");
  rtclm_ipc_fifo_t *ipc = (rtclm_ipc_fifo_t*)calloc(1, sizeof(rtclm_ipc_fifo_t));
  if(!ipc){
    _SE("Failed to allocate memory");
    return NULL;
  }

  _D("Bookkeeping IPC file path");
  ipc->fpath = utils_construct_string("%s/%s", dpath, name);
  if(ipc->fpath== NULL){
    _SE("Failed to construct string");
    goto failure_construct_string;
  }

  _D("Creating FIFO interface");
  if(!access(ipc->fpath, F_OK) == 0){
    if(mkfifo(ipc->fpath, mode) == -1){
      _SE("Failed to create FIFO");
      goto failure_mkfifo;
    }
  }

  _D("Opening FIFO interface: \"%s\"", ipc->fpath);
  ipc->fd = open(ipc->fpath, flags);
  if(ipc->fd == -1){
    _SE("Failed to open FIFO: \"%s\"", ipc->fpath);
    goto failure_open;
  }

  ipc->type  = RTCLM_IPC_FIFO;
  return ipc;


failure_open:
  unlink(ipc->fpath);
failure_mkfifo:
  free(ipc->fpath);
failure_construct_string:
  free(ipc);
  return NULL;
}

void rtclm_ipc_fifo_deinit(rtclm_ipc_fifo_t *ipc){
  close(ipc->fd);
  unlink(ipc->fpath);
  free(ipc->fpath);
  free(ipc);
}


rtclm_ipc_shm_t* rtclm_ipc_shm_init(
  const char  *name,
  mode_t       mode,
  int          flags,
  uint32_t     size)
{

  _D("Allocating memory for RTCLM notifier");
  rtclm_ipc_shm_t *ipc = (rtclm_ipc_shm_t*)calloc(1, sizeof(rtclm_ipc_shm_t));
  if(!ipc){
    _SE("Failed to allocate memory");
    return NULL;
  }

  _D("Bookkeeping IPC file path");
  ipc->fpath = utils_construct_string("/rtclm_%s", name);
  if(ipc->fpath== NULL){
    _SE("Failed to construct string");
    goto failure_construct_string;
  }

  _D("Opening SHM interface");
  ipc->fd = shm_open(ipc->fpath, flags, mode);
  if(ipc->fd == -1){
    _SE("Failed to open SHM: \"%s\"", ipc->fpath);
    goto failure_shm_open;
  }

  _D("Allocating SHM memory for \"%s\"", name);
  int ret = ftruncate(ipc->fd, size);
  if(ret == -1){
    _SE("Failed to truncate shared memory file: \"%s\"", ipc->fpath);
    goto failure_ftruncate;
  }

  _D("Mapping SHM memory for \"%s\", fd: %d", name, ipc->fd);
  ipc->mem = mmap(NULL,
    size,
    PROT_READ | PROT_WRITE,
    MAP_SHARED,
    ipc->fd,
    0);
  if(ipc->mem == ((void*)-1)){
    _SE("Failed to memory map shared memory area for \"%s\"", ipc->fpath);
    goto failure_mmap;
  }

  ipc->size = size;
  return ipc;

failure_mmap:
failure_ftruncate:
  close(ipc->fd);
failure_shm_open:
  free(ipc->fpath);
failure_construct_string:
  free(ipc);
  return NULL;
}


void rtclm_ipc_shm_deinit(rtclm_ipc_shm_t *ipc){
  munmap(ipc->mem, ipc->size);
  close(ipc->fd);
  shm_unlink(ipc->fpath);
  free(ipc->fpath);
  free(ipc);
}
