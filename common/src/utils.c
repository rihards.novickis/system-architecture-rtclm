#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdarg.h>
#include <unistd.h>
#include <errno.h>
#include <dirent.h>
#include <sys/stat.h>

#include "rtclm/utils.h"
#include "rtclm/status.h"
#include "rtclm/logging.h"

char* utils_construct_string(const char *format, ...){
  ssize_t buffer_size;
  char *buffer;
  va_list args;


  /* get size of the allocation */
  va_start(args, format);
  buffer_size = vsnprintf(NULL, 0, format, args);
  va_end(args);
  if(buffer_size < 0){
    _SE("Failed to retreive allocation size");
    return NULL;
  }
  
  /* allocate memory */
  buffer = (char*)malloc(buffer_size+1);
  if(buffer == NULL){
    _SE("Allocation failed");
    return NULL;
  }

  /* generate string */
  va_start(args, format);
  buffer_size = vsnprintf(buffer, buffer_size+1, format, args);
  va_end(args);
  if(buffer_size < 0){
    _SE("Failed to construct string");
    free(buffer);
    return NULL;
  }

  return buffer;
}

rtclmStatus_t utils_mkdir(const char *name){
  DIR *dir = opendir(name);
  if(!dir){
    if(mkdir(name, 0777) == -1){
      _SE("Failed to create directory: \"%s\"", name);
      return RTCLM_FAILED_MKDIR;
    }
  } else {
    closedir(dir);
  }

  return RTCLM_SUCCESS;
}

int utils_dir_exists(const char *name){
  DIR *dir = opendir(name);
  if(dir){
    closedir(dir);
    return 1;
  }

  return 0;
}

int utils_file_exists(const char *name){
  if(access(name, F_OK) == 0){
    return 1;
  }

  return 0;
}
