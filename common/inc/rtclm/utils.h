#ifndef _UTILS_H_
#define _UTILS_H_

/* C-linkage when included from C++ - BEGIN */
#ifdef __cplusplus
  extern "C" {
#endif

#include <stdint.h>
#include "rtclm/status.h"

/* TODO: Documentation */
char* utils_construct_string(const char *format, ...);

/* TODO: Documentation */
rtclmStatus_t utils_mkdir(const char *name);

/* TODO: Documentation */
int utils_dir_exists(const char *name);

/* TODO: Documentation */
int utils_file_exists(const char *name);


static inline uint32_t utils_align_u32(uint32_t size, uint32_t align){
  return (size + align - 1) & (-align);
}

static inline uint64_t utils_align_u64(uint64_t size, uint64_t align){
  return (size + align - 1) & (-align);
}


/* C-linkage when included from C++ - STOP */
#ifdef __cplusplus
  }
#endif

#endif
