#ifndef _CONFIG_H_
#define _CONFIG_H_

#ifndef RTCLM_TMP_DIRECTORY
  #define RTCLM_TMP_DIRECTORY "/tmp/rtclm"
#endif

#ifndef RTCLM_TRACKED_SAMPLE_COUNT
  #define RTCLM_TRACKED_SAMPLE_COUNT 10000
#endif

/* TODO: documentation */
#define FIFO_TMP_DIRECTORY  RTCLM_TMP_DIRECTORY "/fifo"
//#define SHM_TMP_DIRECTORY   RTCLM_TMP_DIRECTORY "/shm"

/* TODO: documentation */
#define AGGR_MAX_NOTES_PER_READ  10

/* TODO: documentation */
#define DISP_HISTOGRAM_BINS          320
#define DISP_HISTOGRAM_STDDEV_RANGE  2


/* TODO: documentation */
#define RTCLM_AGGREGATOR_WAIT_MS  5
#define RTCLM_AGGREGATOR_WAIT_US  (1000*RTCLM_AGGREGATOR_WAIT_MS)


#endif
