#ifndef _RTCLM_STATUS_H_
#define _RTCLM_STATUS_H_

/* Unified RTCLM status flags */
typedef enum{
  RTCLM_SUCCESS = 0,
  RTCLM_FAILURE,
  RTCLM_COMPONENT_EXISTS,
  RTCLM_FAILED_MKDIR,
  RTCLM_FAILED_IPC_INIT,
  RTCLM_NO_MEM,
} rtclmStatus_t;

#endif
