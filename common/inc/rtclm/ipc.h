#ifndef _IPC_H_
#define _IPC_H_

#include <stdint.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

typedef enum{
  RTCLM_IPC_FIFO=0,
  RTCLM_IPC_SHM,
  RTCLM_IPC_COUNT,
} rtclm_ipc_type_t;

/* TODO: Documentation */
//typedef struct {
//  rtclm_ipc_type_t  type;
//  uint8_t           data[];
//} rtclm_ipc_t;

/* TODO: Documentation */
typedef struct {
  rtclm_ipc_type_t  type;
  char             *fpath;
  int               fd;
} rtclm_ipc_fifo_t;

/* TODO: Documentation */
typedef struct {
  rtclm_ipc_type_t  type;
  char             *fpath;
  int               fd;
  void             *mem;
  size_t            size;
} rtclm_ipc_shm_t;


/* TODO: Documentation */
rtclm_ipc_fifo_t* rtclm_ipc_fifo_init(
  const char  *dpath,
  const char  *name,
  mode_t       mode,
  int          flags
);

/* TODO: Documentation */
rtclm_ipc_shm_t* rtclm_ipc_shm_init(
  const char  *name,
  mode_t       mode,
  int          flags,
  uint32_t     size
);


/* TODO: Documentation */
void rtclm_ipc_shm_deinit(rtclm_ipc_shm_t *ipc);

/* TODO: Documentation */
void rtclm_ipc_fifo_deinit(rtclm_ipc_fifo_t *ipc);

#endif
