#ifndef _RTCLM_MESSAGES_H_
#define _RTCLM_MESSAGES_H_

#include <stdint.h>

#define UDP_PAYLOAD_SIZE  508

/* Client-Server commands */
typedef enum {
  RTCLM_CMD_REGISTER=0,
  RTCLM_CMD_UNREGISTER,
  RTCLM_CMD_NOTIFY_LOOP_START,
  RTCLM_CMD_NOTIFY_LOOP_EMIT,
  RTCLM_CMD_NOTIFY_LOOP_STOP,
} command_t;

/* Client->Server->Aggregator notification message types */
typedef enum {
  RTCLM_NOTIFY_EMIT=0,
  RTCLM_NOTIFY_LOOP_START,
  RTCLM_NOTIFY_LOOP_STOP,
} notify_type_t;

/* Client->Server message format */
#pragma pack(push,1)
typedef struct {
  uint16_t  id;
  uint16_t  command;   // important to be 4-byte aligned
  uint8_t   payload[];
} packet_t;
#pragma pack(pop)

/* Server->Aggregator message format */
#pragma pack(push,1)
typedef struct {
  uint16_t  id;
  uint16_t  type;
  uint64_t  timer;
} notify_t;
#pragma pack(pop)

/* Unified SHM status flags */
typedef enum{
  SHM_DEFAULT_STATUS,
  SHM_RESET_STATS,
  SHM_EXPORT_STATS,
} shmStatus_t;

/* Aggregator->(Display) message format */
typedef struct {
  uint64_t maximum;
  uint64_t minimum;
  uint64_t last_timestamp;
  uint64_t last_index;
  uint64_t average; 
  uint64_t std_deviation;
  uint64_t number_of_samples;
  shmStatus_t status;
  uint32_t samples[];
} statistics_t;


#endif
