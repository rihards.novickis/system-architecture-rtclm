#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <arpa/inet.h>

#include "rtclm/server.h"
#include "rtclm/logging.h"
#include "rtclm/messages.h"
#include "rtclm/notifier.h"



void rtclm_server_thread_cleanup(void *pdata){
}

void* rtclm_server_thread(void *pdata){
  rtclmStatus_t status;
  static uint8_t udp_payload[UDP_PAYLOAD_SIZE];
  packet_t *packet;
  struct sockaddr src_addr;
  socklen_t src_addr_length = sizeof(src_addr);
  int size;

  /* Packet */
  packet = (packet_t*)udp_payload;

  /* Retreive private data structure */
  rtclm_server_t *server = (rtclm_server_t*)pdata;

  _D("Registering server thread's cleanup handler");
  pthread_cleanup_push(rtclm_server_thread_cleanup, pdata);

  /* Server loop (NOTE: add mutexes if multiple threads will be used) */
  while(1){
    size = recvfrom(server->socket, packet, UDP_PAYLOAD_SIZE, MSG_WAITALL,
      &src_addr, &src_addr_length);
    if(size == -1){
      _SW("Failed to receive UDP message");
      continue;
    }

    _D("Message received at \"*:%u\" from \"%u\" (%d bytes)",
       server->port, ((struct sockaddr_in*)&src_addr)->sin_port, size);

    switch(packet->command){
      case RTCLM_CMD_REGISTER:
        _D("Registering client with string id: \"%s\"", (char*)packet->payload);
        status = rtclm_register(server->notifier, (uint16_t*)packet->payload, (char*)packet->payload);
        if(status != RTCLM_SUCCESS){
          _W("Failed to register client");
          break;
        }

        _D("Acknowledging registration with assigned id: %u", *(uint16_t*)packet->payload);
        sendto(server->socket, packet, size, MSG_CONFIRM,
          &src_addr, src_addr_length);
        break;


      case RTCLM_CMD_UNREGISTER:
        _D("Unregistering client with id: %u", *(uint16_t*)packet->payload);
        status = rtclm_unregister(server->notifier, *(uint16_t*)packet->payload);
        if(status != RTCLM_SUCCESS){
          _W("Failed to unregister client");
          break;
        }
        
        _D("Acknowledging unregister of client with id: %u", *(uint16_t*)packet->payload);
        sendto(server->socket, packet, size, MSG_CONFIRM,
          &src_addr, src_addr_length);
        break;


      case RTCLM_CMD_NOTIFY_LOOP_START:
      case RTCLM_CMD_NOTIFY_LOOP_EMIT:
      case RTCLM_CMD_NOTIFY_LOOP_STOP:
      {

          /* retreive communication interface */
          rtclm_notifier_iface_t *interface = server->notifier->ifaces[packet->id];

          /* construct message */
          notify_t message = {
            .id    = packet->id,
            .type  = packet->command,
            .timer = *(uint64_t*)(packet->payload),
          };

          /* send via FIFO */
          int ret = write(interface->ipc->fd, &message, sizeof(message));
          if(ret != sizeof(message)){
            _W("Failed to write into FIFO");
          }
        }
        break;

      default:
        break;
    }
  }

  /* Pthread's cleanup handler */
  pthread_cleanup_pop(1);

  return NULL;
}

rtclm_server_t* rtclm_server_init(uint16_t port){
  rtclm_server_t *server;
  int ret;

  _D("Initializing RTCLM server at \"*:%u\"", port);
  server = malloc(sizeof(rtclm_server_t));
  if(server == NULL){
    _SE("Failed to initialize server");
    return NULL;
  }

  _D("Initializing notifier");
  server->notifier = rtclm_notifier_init();
  if(server->notifier == NULL){
    _E("Failed to initialize notifier");
    goto failure_notifier_init;
  }

  _D("Storing connection attributes \"*:%u\"", port);
  server->addr.sin_addr.s_addr = INADDR_ANY;
  server->port                 = port;
  server->addr.sin_family      = AF_INET;
  server->addr.sin_port        = htons(port); 

  _D("Initializing socket for \"*:%u\"", port); 
  server->socket = socket(AF_INET, SOCK_DGRAM, 0);
  if(server->socket == -1){
    _SE("Failed to create socket\n");
    goto failure_socket;
  }

  _D("Binding server at \"*:%u\"", port);
  if(bind(server->socket, (const struct sockaddr*)&server->addr,
    sizeof(server->addr)) < 0 ){ 
    _SE("Failed to bind server");
    goto failure_bind;
  } 

  _D("Starting server thread at \"%u\"", port);
  ret = pthread_create(&server->pid, 0, rtclm_server_thread, server);
  if(ret != 0){
    _E("Failed to initialize server pthread (error: %d)", ret);
    goto failure_pthread_create;
  }

  return server;

failure_pthread_create:
failure_bind:
  shutdown(server->socket, SHUT_RDWR);
failure_socket:
  rtclm_notifier_deinit(server->notifier);
failure_notifier_init:
  free(server);
  return NULL;
}

void rtclm_server_deinit(rtclm_server_t *server){
  _D("Deinitializing RTCLM sever at \"*:%u\"", server->port);
  pthread_cancel(server->pid);
  shutdown(server->socket, SHUT_RDWR);
  close(server->socket);
  rtclm_notifier_deinit(server->notifier);
  free(server);
}
