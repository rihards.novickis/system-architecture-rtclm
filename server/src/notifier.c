#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <dirent.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>
#include <fcntl.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "rtclm/notifier.h"
#include "rtclm/messages.h"
#include "rtclm/logging.h"
#include "rtclm/status.h"
#include "rtclm/config.h"
#include "rtclm/utils.h"
#include "rtclm/ipc.h"


/********************* LLIST API - BEGIN *********************/
static int llist_compare_by_name(rtclm_notifier_iface_t *component, const void *pdata){
  char *name = (char*)pdata;
  return (strcmp(component->name, pdata) == 0) ? 1 : 0;
}


rtclm_notifier_iface_t* llist_find(
  rtclm_notifier_t *notifier,
  int(*compare_handler)(rtclm_notifier_iface_t*,const void*),
  const void *pdata)
{
  /* Find component */
  for(uint16_t i=0; i<notifier->counter_max_registered; i++){
    if(compare_handler(notifier->ifaces[i], pdata)){
      return notifier->ifaces[i];
    }
  }

  return NULL;
}
/********************* LLIST API - END *********************/


static inline int interface_exists(const char *name){
  char fpath[256];

  _D("Checking if notifier directory exists");
  DIR *dir = opendir(FIFO_TMP_DIRECTORY);
  if(!dir){
    return 0;
  }

  snprintf(fpath, sizeof(fpath), FIFO_TMP_DIRECTORY"/%s", name);
  if(!access(fpath, F_OK) == 0) {
    return 0;
  }
  
  return 1;
}


rtclmStatus_t rtclm_register(rtclm_notifier_t *notifier, uint16_t *id, const char *name){
  rtclm_notifier_iface_t *interface;
  char fpath[256];
  int fifo_exists = 0;

  _D("Checking and creating RTCLM directory");
  if(utils_mkdir(RTCLM_TMP_DIRECTORY) != RTCLM_SUCCESS){
    return RTCLM_FAILED_MKDIR;
  }

  _D("Checking and creating FIFO directory");
  if(utils_mkdir(FIFO_TMP_DIRECTORY) != RTCLM_SUCCESS){
    return RTCLM_FAILED_MKDIR;
  }

  _D("Checking for already registered comms");
  interface = llist_find(notifier, llist_compare_by_name, name);
  if(interface != NULL){
    *id = interface->id;
    return RTCLM_SUCCESS;
  }

  _D("Checking if FIFO for an unregisted component already exists");
  if(interface_exists(name)){
    fifo_exists = 1;
  }

  _D("Allocating memory for a new interface");
  interface = (rtclm_notifier_iface_t*)malloc(sizeof(rtclm_notifier_iface_t));
  if(interface == NULL){
    _SE("Failed to allocate memory");
    return RTCLM_NO_MEM;
  }

  _D("Initializing FIFO IPC");
  interface->ipc = rtclm_ipc_fifo_init(
    FIFO_TMP_DIRECTORY,
    name,
    0777,
    O_RDWR | O_NONBLOCK);

  if(interface->ipc == NULL){
    _E("Failed to initilize FIFO IPC");
    goto failure_rtclm_ipc_fifo_init;
  }

  _D("Bookkeeping interface name");
  interface->name = strdup(name);
  if(interface->name == NULL){
    _SE("Failed to allocate memory");
    goto failure_strdup;
  }

  /* book keeping */
  notifier->counter_max_registered++;
  notifier->ifaces = (rtclm_notifier_iface_t**)realloc(
    notifier->ifaces, notifier->counter_max_registered*sizeof(void*));

  if(notifier->ifaces == NULL){
    _SE("Failed to allcoate memory");
    goto failure_realloc;
  }

  notifier->ifaces[notifier->counter_id] = interface;

  /* increment notifier component ids */
  interface->id = notifier->counter_id++;

  /* update output arguments */
  *id = interface->id;

  notifier->counter_registered++;
  return RTCLM_SUCCESS;


failure_realloc:
  free(interface->name);
failure_strdup:
  rtclm_ipc_fifo_deinit(interface->ipc);
failure_rtclm_ipc_fifo_init:
  free(interface);
failure_malloc:

  return RTCLM_FAILURE;
}


rtclmStatus_t rtclm_unregister(rtclm_notifier_t *notifier, uint16_t id){

  _D("Finding component using ID: %u", id);
  rtclm_notifier_iface_t *interface = notifier->ifaces[id];
  if(interface == NULL){
    _E("Failed to unregister component with ID=%u", id);
    return RTCLM_FAILURE;
  }

  _D("Removing FIFO interface \"%s\"", interface->name);
  rtclm_ipc_fifo_deinit(interface->ipc);
  free(interface->name);
  interface->name = NULL;
  return RTCLM_SUCCESS;
}

rtclmStatus_t rtclm_notify(rtclm_notifier_t *notifier, uint16_t id, notify_type_t type){
  notify_t message;
  struct timespec tv;
  rtclm_notifier_iface_t *interface;

  /* Find component node by its id */
  interface = notifier->ifaces[id];
  if(interface == NULL){
    _E("Failed to identify notification client");
    return RTCLM_FAILURE;
  }
  
  /* Get timestamp */
  if(clock_gettime(CLOCK_REALTIME, &tv)){
    _E("Failed to retreive timestamp");
    return RTCLM_FAILURE;
  }

  /* Construct notification message */
  message.id    = id;
  message.type  = type;
  message.timer = 1e9*tv.tv_sec + tv.tv_nsec;

  /* Send notify message */
  int ret = write(interface->ipc->fd, &message, sizeof(message));
  if(ret == -1){
    _SE("Failed to write to FIFO");
    return RTCLM_FAILURE;
  } else if (ret != sizeof(message)){
    _E("Failed to write all data to FIFO (%u bytes written)", ret);
    return RTCLM_FAILURE;
  }

  return RTCLM_SUCCESS;
}

rtclm_notifier_t* rtclm_notifier_init(){
  rtclm_notifier_t *notifier;

  _D("Initializing notifier");
  notifier = calloc(1, sizeof(rtclm_notifier_t));
  if(notifier == NULL){
    _SE("Failed to allocate memory");
    return NULL;
  }

  return notifier;
}

void rtclm_notifier_deinit(rtclm_notifier_t *notifier){
  /* TODO: deinitialize each IPC individually */
  for(int i=0; i<notifier->counter_max_registered; i++){
    if(notifier->ifaces[i]->name){
      rtclm_unregister(notifier, notifier->ifaces[i]->id);
    }
  }

  free(notifier->ifaces); // TODO: potential segfault
  free(notifier);
}
