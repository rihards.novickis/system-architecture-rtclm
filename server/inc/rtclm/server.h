#ifndef _RTCLM_SERVER_H_
#define _RTCLM_SERVER_H_

/* C-linkage when included from C++ - BEGIN */
#ifdef __cplusplus
  extern "C" {
#endif


#include <stdint.h>
#include <pthread.h>
#include <arpa/inet.h>
#include "rtclm/status.h"
#include "rtclm/notifier.h"

/* RTCLM server's struct object */
typedef struct {
  uint16_t            port;
  int                 socket;
  struct sockaddr_in  addr;
  pthread_t           pid;
  rtclm_notifier_t   *notifier;
} rtclm_server_t;

/* TODO: documentation */
rtclm_server_t* rtclm_server_init(uint16_t port);

/* TODO: documentation */
void rtclm_server_deinit(rtclm_server_t *server);


/* C-linkage when included from C++ - STOP */
#ifdef __cplusplus
  }
#endif

#endif
