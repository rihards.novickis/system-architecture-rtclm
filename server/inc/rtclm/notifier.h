#ifndef _NOTIFIER_H_
#define _NOTIFIER_H_

/* C-linkage when included from C++ - BEGIN */
#ifdef __cplusplus
  extern "C" {
#endif

#include <stdint.h>
#include "rtclm/status.h"
#include "rtclm/messages.h"
#include "rtclm/ipc.h"

typedef struct {
  char              *name;
  uint16_t           id;
  rtclm_ipc_fifo_t  *ipc;
} rtclm_notifier_iface_t;

/* TODO: Documentation */
typedef struct {
  rtclm_notifier_iface_t **ifaces;
  uint16_t                 counter_id;
  uint16_t                 counter_registered;
  uint16_t                 counter_max_registered;
} rtclm_notifier_t;


/* TODO: Documentation */
rtclm_notifier_t* rtclm_notifier_init();

/* TODO: Documentation */
void rtclm_notifier_deinit(rtclm_notifier_t *notifier);


/* TODO: Documentation */
rtclmStatus_t rtclm_register(rtclm_notifier_t *notifier, uint16_t *id, const char *name);

/* TODO: Documentation */
rtclmStatus_t rtclm_unregister(rtclm_notifier_t *notifier, uint16_t id);

/* TODO: Documentation */
rtclmStatus_t rtclm_notify(rtclm_notifier_t *notifier, uint16_t id, notify_type_t type);


/* C-linkage when included from C++ - STOP */
#ifdef __cplusplus
  }
#endif

#endif
