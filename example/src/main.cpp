#include <stdio.h>
#include <stdint.h>
#include <pthread.h>
#include <unistd.h>
#include "rtclm/client.h"
#include "rtclm/server.h"
#include "rtclm/display.h"
#include "rtclm/logging.h"
#include "rtclm/aggregator.h"


#define RTCLM_PORT  7788
#define RTCLM_IP    "127.0.0.1"


typedef struct{
  uint64_t      delay_us;
  uint16_t      rtclm_port;
  const char   *rtclm_ip;
  const char   *rtclm_name;
} pthread_pdata_t;


void *pthread_entry(void *args){
  pthread_pdata_t *pdata = (pthread_pdata_t*)args;
  rtclm_client_t  *client;

  client = rtclm_client_init(pdata->rtclm_name, pdata->rtclm_ip, pdata->rtclm_port);
  if(client == NULL){
    _E("Failed to initialize RTCLM client");
    return (void*)-1;
  }

  while(1){
    rtclm_client_notify_emit(client);

    //_I("%s: Simulating work (%lu)",pdata->rtclm_name, pdata->delay_us);
    usleep(pdata->delay_us);
  }

  rtclm_client_deinit(client);
  return (void*)0;
}



pthread_pdata_t configs[] = {
 {1000000, RTCLM_PORT, RTCLM_IP, "Component_1s"},
 { 100000, RTCLM_PORT, RTCLM_IP, "Component_100ms"},
 {  10000, RTCLM_PORT, RTCLM_IP, "Component_10ms"},
 {   5000, RTCLM_PORT, RTCLM_IP, "Component_5ms"},
 {   3000, RTCLM_PORT, RTCLM_IP, "Component_3ms"},
 {   1000, RTCLM_PORT, RTCLM_IP, "Component_1ms"},
};

#define CONFIG_COUNT (sizeof(configs)/sizeof(*configs))


int main(void){
  rtclm_server_t     *server;
  rtclm_aggregator_t *aggregator;
  rtclm_display_t    *display;
  pthread_t pids[CONFIG_COUNT];

  _I("Initializing RTCLM server");
  server = rtclm_server_init(RTCLM_PORT);
  if(server == NULL){
    _E("Failed to initialize RTCLM server");
    return 1;
  }

  _I("Initializing RTCLM aggregator");
  aggregator = rtclm_aggregator_init();
  if(aggregator == NULL){
    _E("Failed to initialize RTCLM aggregator");
    return 1;
  }

  _I("Initializing RTCLM display");
  display = rtclm_display_init();
  if(display == NULL){
    _E("Failed to initialize RTCLM display");
    return 1;
  }

  for(int i=0; i<CONFIG_COUNT; i++){
    _I("Initializing component thread: %s", configs[i].rtclm_name);
    int ret = pthread_create(&pids[i], NULL, pthread_entry, &configs[i]);
    if(ret == -1){
      _E("Failed to initialize pthread: %s", configs[i].rtclm_name);
      return 1;
    }
  }

  for(int i=0; i<CONFIG_COUNT; i++){
    _I("Waiting for component: %s", configs[i].rtclm_name);
    pthread_join(pids[i], NULL);
  }

  _I("Deinitializing RTCLM display");
  rtclm_display_deinit(display);

  _I("Deinitializing RTCLM aggregator");
  rtclm_aggregator_deinit(aggregator);

  _I("Deinitializing RTCLM server");
  rtclm_server_deinit(server);

  return 0;
}
