#include <stdlib.h>
#include <stdio.h>
#include "gtest/gtest.h"
#include "rtclm/server.h"
#include "rtclm/logging.h"

#define RTCLM_SERVER_IP    "127.0.0.1"
#define RTCLM_SERVER_PORT  9999

TEST(server, initialization){
  rtclm_server_t *server;

  _D("Initializing RTCLM server");
  server = rtclm_server_init(RTCLM_SERVER_PORT);

  EXPECT_TRUE(server != NULL);
  EXPECT_EQ(server->port, RTCLM_SERVER_PORT);
  EXPECT_GT(server->socket, 0);

  _D("Deinitializing RTCLM server");
  rtclm_server_deinit(server);
}
