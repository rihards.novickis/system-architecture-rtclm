#include <stdlib.h>
#include <stdio.h>
#include "gtest/gtest.h"
#include "rtclm/client.h"
#include "rtclm/server.h"
#include "rtclm/logging.h"

#define RTCLM_SERVER_IP    "127.0.0.1"
#define RTCLM_SERVER_PORT  10000
#define COMPONENT_NAME     "gtest_test_0"

TEST(commands, register_client){
  rtclm_server_t *server;
  rtclm_client_t *client;

  _D("Initializing RTCLM server");
  server = rtclm_server_init(RTCLM_SERVER_PORT);
  EXPECT_TRUE(server != NULL);

  _D("Initializing RTCLM client");
  client = rtclm_client_init(COMPONENT_NAME, RTCLM_SERVER_IP, RTCLM_SERVER_PORT);
  EXPECT_TRUE(client != NULL);

  _D("Deinitializing RTCLM client");
  rtclm_client_deinit(client);

  _D("Deinitializing RTCLM server");
  rtclm_server_deinit(server);
}
