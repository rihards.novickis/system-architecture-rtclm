#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include "gtest/gtest.h"
#include "rtclm/logging.h"
#include "rtclm/notifier.h"

TEST(notifier, register_single){
  rtclm_notifier_t *notifier;
  rtclmStatus_t status;
  uint16_t id;

  _D("Initializing RTCLM notifier");
  notifier = rtclm_notifier_init();
  EXPECT_TRUE(notifier != NULL);

  status = rtclm_register(notifier, &id, "gtest_test_1");
  EXPECT_EQ(status, RTCLM_SUCCESS);

  /* id depends on a shared variable, cannot be tested when gtest compiled with
   * pthreads */
  //EXPECT_EQ(id, 0);

  status = rtclm_unregister(notifier, id);
  EXPECT_EQ(status, RTCLM_SUCCESS);

  _D("Deitializing RTCLM notifier");
  rtclm_notifier_deinit(notifier);
}

TEST(notifier, register_multiple){
  rtclm_notifier_t *notifier;
  rtclmStatus_t status;
  uint16_t id[10];
  const char *names[] = {
    "t0","t1","t2","t3","t4",
    "t5","t6","t7","t8","t9"};

  _D("Initializing RTCLM notifier");
  notifier = rtclm_notifier_init();
  EXPECT_TRUE(notifier != NULL);

  for(int i=0; i<sizeof(id)/sizeof(*id); i++){
    status = rtclm_register(notifier, &id[i], names[i]);
    EXPECT_EQ(status, RTCLM_SUCCESS);
    /* id depends on a shared variable, cannot be tested when gtest compiled with
     * pthreads */
    // EXPECT_EQ(id[i], i);
  }

  for(int i=0; i<sizeof(id)/sizeof(*id); i++){
    status = rtclm_unregister(notifier, id[i]);
    EXPECT_EQ(status, RTCLM_SUCCESS);
  }

  _D("Deitializing RTCLM notifier");
  rtclm_notifier_deinit(notifier);
}
