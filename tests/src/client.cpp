#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include "gtest/gtest.h"
#include "rtclm/client.h"
#include "rtclm/server.h"
#include "rtclm/logging.h"
#include "rtclm/utils.h"

#define RTCLM_SERVER_IP    "127.0.0.1"
#define RTCLM_SERVER_PORT  9999
#define COMPONENT_NAME     "gtest_test_2"


void *pthread_client_entry(void *args){
  const char *name = (const char*)args;

  _D("Initializing RTCLM client: %s", name);
  rtclm_client_t *client = rtclm_client_init(name, RTCLM_SERVER_IP, RTCLM_SERVER_PORT);
  EXPECT_TRUE(client != NULL);
  EXPECT_STREQ(client->name, name);
  EXPECT_STREQ(client->ip, RTCLM_SERVER_IP);
  EXPECT_EQ(client->port, RTCLM_SERVER_PORT);
  EXPECT_GT(client->socket, 0);

  return client;
}


TEST(client, init){
  rtclm_client_t *client;
  rtclm_server_t *server;

  _D("Initializing RTCLM server");
  server = rtclm_server_init(RTCLM_SERVER_PORT);
  EXPECT_TRUE(server != NULL);

  _D("Initializing RTCLM client");
  client = rtclm_client_init(COMPONENT_NAME, RTCLM_SERVER_IP, RTCLM_SERVER_PORT);

  EXPECT_TRUE(client != NULL);
  EXPECT_STREQ(client->name, COMPONENT_NAME);
  EXPECT_STREQ(client->ip, RTCLM_SERVER_IP);
  EXPECT_EQ(client->port, RTCLM_SERVER_PORT);
  EXPECT_GT(client->socket, 0);

  _D("Deinitializing RTCLM client");
  rtclm_client_deinit(client);

  _D("Deinitializing RTCLM server");
  rtclm_server_deinit(server);
}

#define CLIENT_COUNT 10
TEST(client, init_multiple){
  rtclm_client_t *clients[CLIENT_COUNT];
  rtclm_server_t *server;
  pthread_t  pids[CLIENT_COUNT];
  char      *client_names[CLIENT_COUNT];

  _D("Initializing RTCLM server");
  server = rtclm_server_init(RTCLM_SERVER_PORT);
  EXPECT_TRUE(server != NULL);


  _D("Initializing RTCLM clients");
  for(int i=0; i<CLIENT_COUNT; i++){
    client_names[i] = utils_construct_string("rtclm_test_%d", i);
    EXPECT_TRUE(client_names[i] != NULL);
    int ret = pthread_create(&pids[i], 0, pthread_client_entry, client_names[i]);
    EXPECT_EQ(ret, 0);
  }

  _D("Waiting for RTCLM client initialization threads to complete");
  for(int i=0; i<CLIENT_COUNT; i++){
    int ret = pthread_join(pids[i], (void**)&clients[i]);
    EXPECT_EQ(ret, 0);
  }

  _D("Checking client IDs for duplicates");
  for(int i=0;   i<CLIENT_COUNT; i++){
    for(int j=i+1; j<CLIENT_COUNT; j++){
      EXPECT_NE(clients[i]->id, clients[j]->id) << clients[i]->name << " vs " << clients[j]->name;
  }}

  _D("Deinitializing RTCLM clients");
  for(int i=0; i<CLIENT_COUNT; i++){
    rtclm_client_deinit(clients[i]);
    free(client_names[i]);
  }

  _D("Deinitializing RTCLM server");
  rtclm_server_deinit(server);
}
