#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

#include "gtest/gtest.h"
#include "rtclm/aggregator.h"
#include "rtclm/logging.h"
#include "rtclm/status.h"
#include "rtclm/utils.h"
#include "rtclm/notifier.h"
#include "rtclm/config.h"
#include "rtclm/ipc.h"

#define AGGREGATOR_WAIT_TIME_US  10000


TEST(aggregator, initialization){
  rtclm_aggregator_t *aggregator;

  _D("Initializing RTCLM aggregator");
  aggregator = rtclm_aggregator_init();
  EXPECT_TRUE(aggregator != NULL);
  EXPECT_NE(aggregator->pid, 0);

  usleep(AGGREGATOR_WAIT_TIME_US);

  _D("Deitializing RTCLM aggregator");
  rtclm_aggregator_deinit(aggregator);
}


TEST(aggregator, registration){
  rtclm_aggregator_t *aggregator;
  rtclm_notifier_t   *notifier;
  uint16_t ids[4];

  _D("Initializing RTCLM notifier");
  notifier = rtclm_notifier_init();
  EXPECT_TRUE(notifier != NULL);

  _D("Initializing RTCLM aggregator");
  aggregator = rtclm_aggregator_init();
  EXPECT_TRUE(aggregator != NULL);
  EXPECT_NE(aggregator->pid, 0);

  /* give some time for aggregator to initialize */
  usleep(AGGREGATOR_WAIT_TIME_US);

  EXPECT_EQ(RTCLM_SUCCESS, rtclm_register(notifier, &ids[0], "test_aggregator_0"));
  EXPECT_EQ(RTCLM_SUCCESS, rtclm_register(notifier, &ids[1], "test_aggregator_1"));
  EXPECT_EQ(RTCLM_SUCCESS, rtclm_register(notifier, &ids[2], "test_aggregator_2"));
  EXPECT_EQ(RTCLM_SUCCESS, rtclm_register(notifier, &ids[3], "test_aggregator_3"));
  EXPECT_TRUE(utils_file_exists("/tmp/rtclm/fifo/test_aggregator_0"));
  EXPECT_TRUE(utils_file_exists("/tmp/rtclm/fifo/test_aggregator_1"));
  EXPECT_TRUE(utils_file_exists("/tmp/rtclm/fifo/test_aggregator_2"));
  EXPECT_TRUE(utils_file_exists("/tmp/rtclm/fifo/test_aggregator_3"));

  /* give some time for aggregator to aggregate */
  usleep(AGGREGATOR_WAIT_TIME_US);

  /* aggregator should have created the corresponding shm nodes */
  EXPECT_TRUE(utils_file_exists("/dev/shm/rtclm_test_aggregator_0"));
  EXPECT_TRUE(utils_file_exists("/dev/shm/rtclm_test_aggregator_1"));
  EXPECT_TRUE(utils_file_exists("/dev/shm/rtclm_test_aggregator_2"));
  EXPECT_TRUE(utils_file_exists("/dev/shm/rtclm_test_aggregator_3"));

  /* remove FIFOs */
  EXPECT_EQ(RTCLM_SUCCESS, rtclm_unregister(notifier, ids[0]));
  EXPECT_EQ(RTCLM_SUCCESS, rtclm_unregister(notifier, ids[1]));
  EXPECT_EQ(RTCLM_SUCCESS, rtclm_unregister(notifier, ids[2]));
  EXPECT_EQ(RTCLM_SUCCESS, rtclm_unregister(notifier, ids[3]));
  EXPECT_FALSE(utils_file_exists("/tmp/rtclm/fifo/test_aggregator_0"));
  EXPECT_FALSE(utils_file_exists("/tmp/rtclm/fifo/test_aggregator_1"));
  EXPECT_FALSE(utils_file_exists("/tmp/rtclm/fifo/test_aggregator_2"));
  EXPECT_FALSE(utils_file_exists("/tmp/rtclm/fifo/test_aggregator_3"));

  _D("Deitializing RTCLM aggregator");
  rtclm_aggregator_deinit(aggregator);

  /* check if shm nodes have been removed */
  EXPECT_FALSE(utils_file_exists("/dev/shm/rtclm_test_aggregator_0"));
  EXPECT_FALSE(utils_file_exists("/dev/shm/rtclm_test_aggregator_1"));
  EXPECT_FALSE(utils_file_exists("/dev/shm/rtclm_test_aggregator_2"));
  EXPECT_FALSE(utils_file_exists("/dev/shm/rtclm_test_aggregator_3"));

  _D("Deitializing RTCLM notifier");
  rtclm_notifier_deinit(notifier);
}

void write_emit(rtclm_ipc_fifo_t *fifo, uint64_t value){
  notify_t message = {0, RTCLM_CMD_NOTIFY_LOOP_EMIT, value};
  int ret = write(fifo->fd, &message, sizeof(message));
  EXPECT_EQ(ret, sizeof(message));
}

void write_start(rtclm_ipc_fifo_t *fifo, uint64_t value){
  notify_t message = {0, RTCLM_CMD_NOTIFY_LOOP_START, value};
  int ret = write(fifo->fd, &message, sizeof(message));
  EXPECT_EQ(ret, sizeof(message));
}

void write_stop(rtclm_ipc_fifo_t *fifo, uint64_t value){
  notify_t message = {0, RTCLM_CMD_NOTIFY_LOOP_STOP, value};
  int ret = write(fifo->fd, &message, sizeof(message));
  EXPECT_EQ(ret, sizeof(message));
}


void check_stats(statistics_t *stats, uint64_t max, uint64_t min, uint64_t lt, uint64_t li, uint64_t avg, uint64_t std, uint64_t scount){
  EXPECT_EQ(stats->maximum,           max);
  EXPECT_EQ(stats->minimum,           min);
  EXPECT_EQ(stats->last_timestamp,    lt);
  EXPECT_EQ(stats->last_index,        li);
  EXPECT_EQ(stats->average,           avg);
  EXPECT_EQ(stats->std_deviation,     std);
  EXPECT_EQ(stats->number_of_samples, scount);
}

TEST(aggregator, emit){
  rtclm_aggregator_t *aggregator;
  rtclm_ipc_fifo_t *fifo;
  rtclm_ipc_shm_t *shm;
  int ret;

  aggregator = rtclm_aggregator_init();
  EXPECT_TRUE(aggregator != NULL);
  EXPECT_NE(aggregator->pid, 0);

  fifo = rtclm_ipc_fifo_init(FIFO_TMP_DIRECTORY, "test", 0777, O_RDWR | O_NONBLOCK);
  EXPECT_TRUE(fifo != NULL);

  /* aggregator should have created the corresponding shm nodes */
  usleep(AGGREGATOR_WAIT_TIME_US);
  EXPECT_TRUE(utils_file_exists("/dev/shm/rtclm_test"));

  uint32_t shm_size = utils_align_u32(
    sizeof(statistics_t)+sizeof(uint64_t)*RTCLM_TRACKED_SAMPLE_COUNT, 0x1000);
  shm = rtclm_ipc_shm_init("test", S_IRUSR | S_IWUSR, O_RDWR | O_CREAT, shm_size);
  EXPECT_TRUE(shm != NULL);
  statistics_t *stats = (statistics_t*)shm->mem;

  write_emit(fifo, 100); /* initial write */
  write_emit(fifo, 200); /* duration 100 */

  usleep(AGGREGATOR_WAIT_TIME_US);
  check_stats(stats, 100, 100, 200, 0, 100, 0, 1);

  write_emit(fifo, 400); /* duration 200 */

  usleep(AGGREGATOR_WAIT_TIME_US);
  check_stats(stats, 200, 100, 400, 1, 150, 50, 2);
  EXPECT_EQ(stats->samples[0],        100);
  EXPECT_EQ(stats->samples[1],        200);

  _D("Deitializing RTCLM aggregator");
  rtclm_aggregator_deinit(aggregator);
  rtclm_ipc_fifo_deinit(fifo);
  rtclm_ipc_shm_deinit(shm);
}


TEST(aggregator, emit_multiple){
  rtclm_aggregator_t *aggregator;
  rtclm_ipc_fifo_t *fifo[2];
  rtclm_ipc_shm_t *shm[2];
  statistics_t *stats[2];
  int ret;

  aggregator = rtclm_aggregator_init();
  EXPECT_TRUE(aggregator != NULL);
  EXPECT_NE(aggregator->pid, 0);

  fifo[0] = rtclm_ipc_fifo_init(FIFO_TMP_DIRECTORY, "test_0", 0777, O_RDWR | O_NONBLOCK);
  fifo[1] = rtclm_ipc_fifo_init(FIFO_TMP_DIRECTORY, "test_1", 0777, O_RDWR | O_NONBLOCK);
  EXPECT_TRUE(fifo[0] != NULL);
  EXPECT_TRUE(fifo[1] != NULL);

  /* aggregator should have created the corresponding shm nodes */
  usleep(AGGREGATOR_WAIT_TIME_US);
  EXPECT_TRUE(utils_file_exists("/dev/shm/rtclm_test_0"));
  EXPECT_TRUE(utils_file_exists("/dev/shm/rtclm_test_1"));

  uint32_t shm_size = utils_align_u32(
    sizeof(statistics_t)+sizeof(uint64_t)*RTCLM_TRACKED_SAMPLE_COUNT, 0x1000);
  shm[0] = rtclm_ipc_shm_init("test_0", S_IRUSR | S_IWUSR, O_RDWR | O_CREAT, shm_size);
  shm[1] = rtclm_ipc_shm_init("test_1", S_IRUSR | S_IWUSR, O_RDWR | O_CREAT, shm_size);
  EXPECT_TRUE(shm[0] != NULL);
  EXPECT_TRUE(shm[1] != NULL);
  stats[0] = (statistics_t*)shm[0]->mem;
  stats[1] = (statistics_t*)shm[1]->mem;

  write_emit(fifo[0], 100); /* initial write */
  write_emit(fifo[0], 200); /* duration 100 */
  write_emit(fifo[1], 100); /* initial write */
  write_emit(fifo[1], 200); /* duration 100 */

  usleep(AGGREGATOR_WAIT_TIME_US);
  check_stats(stats[0], 100, 100, 200, 0, 100, 0, 1);
  check_stats(stats[1], 100, 100, 200, 0, 100, 0, 1);

  write_emit(fifo[0], 400); /* duration 200 */
  write_emit(fifo[1], 400); /* duration 200 */

  usleep(AGGREGATOR_WAIT_TIME_US);
  check_stats(stats[0], 200, 100, 400, 1, 150, 50, 2);
  check_stats(stats[1], 200, 100, 400, 1, 150, 50, 2);
  EXPECT_EQ(stats[0]->samples[0],        100);
  EXPECT_EQ(stats[0]->samples[1],        200);
  EXPECT_EQ(stats[1]->samples[0],        100);
  EXPECT_EQ(stats[1]->samples[1],        200);

  _D("Deitializing RTCLM aggregator");
  rtclm_aggregator_deinit(aggregator);
  rtclm_ipc_fifo_deinit(fifo[0]);
  rtclm_ipc_fifo_deinit(fifo[1]);
  rtclm_ipc_shm_deinit(shm[0]);
  rtclm_ipc_shm_deinit(shm[1]);
}


TEST(aggregator, start_stop){
  rtclm_aggregator_t *aggregator;
  rtclm_ipc_fifo_t *fifo;
  rtclm_ipc_shm_t *shm;
  notify_t message;
  int ret;

  aggregator = rtclm_aggregator_init();
  EXPECT_TRUE(aggregator != NULL);
  EXPECT_NE(aggregator->pid, 0);

  fifo = rtclm_ipc_fifo_init(FIFO_TMP_DIRECTORY, "test", 0777, O_RDWR | O_NONBLOCK);
  EXPECT_TRUE(fifo != NULL);

  /* aggregator should have created the corresponding shm nodes */
  usleep(AGGREGATOR_WAIT_TIME_US);
  EXPECT_TRUE(utils_file_exists("/dev/shm/rtclm_test"));

  uint32_t shm_size = utils_align_u32(
    sizeof(statistics_t)+sizeof(uint64_t)*RTCLM_TRACKED_SAMPLE_COUNT, 0x1000);
  shm = rtclm_ipc_shm_init("test", S_IRUSR | S_IWUSR, O_RDWR | O_CREAT, shm_size);
  EXPECT_TRUE(shm != NULL);
  statistics_t *stats = (statistics_t*)shm->mem;

  /* duration 100 */
  write_start(fifo, 100);
  write_stop (fifo, 200);

  usleep(AGGREGATOR_WAIT_TIME_US);
  check_stats(stats, 100, 100, 200, 0, 100, 0, 1);
  EXPECT_EQ(stats->samples[0],        100);


  /* duration 200 */
  write_start(fifo, 300);
  write_stop (fifo, 500);

  usleep(AGGREGATOR_WAIT_TIME_US);
  check_stats(stats, 200, 100, 500, 1, 150, 50, 2);
  EXPECT_EQ(stats->samples[0],        100);
  EXPECT_EQ(stats->samples[1],        200);

  _D("Deitializing RTCLM aggregator");
  rtclm_aggregator_deinit(aggregator);
}
